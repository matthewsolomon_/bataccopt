import { createRouter, createWebHistory } from 'vue-router'
import TransliteratorView from '../views/TransliteratorView.vue' 
import BatakNeoTrans from '../views/BatakNeoTrans.vue'
import ComingSoon from '../views/ComingSoon.vue'


const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'transliterator',
      component: TransliteratorView
    },
    {
      path: '/batakneo',
      name: 'batakneo',
      component: BatakNeoTrans
    },
    {
      path: '/cs',
      name: 'comingsoon',
      component: ComingSoon
    }
  ]
})

export default router
