type Batak = [string,  string]


const SpecialOneScriptOne : Batak[] = [
    ["\u1BE0", "nya"],
    ["\u1BE0\u1BEA", "nyi"],
    ["\u1BE0\u1BE7", "nyu"],
    ["\u1BE0\u1BE9", "nye"],
    ["\u1BE0\u1BEC", "nyo"],

    ["\u1BDD", "nga"],
    ["\u1BDD\u1BEA", "ngi"],
    ["\u1BDD\u1BE7", "ngu"],
    ["\u1BDD\u1BE9", "nge"],
    ["\u1BDD\u1BEC", "ngo"],
    
]

const SpecialOneScriptTwo : Batak[] = [
    ["\u1BE0\u1BF2", "ny"],
    ["\u1BDD\u1BF2", "ng"],
    ["\u1BF0\u0020", "ng "],
]

const SpecialScript : Batak[] = [
    ["\u1BC2", "ha"],
    ["\u1BC2", "ka"],
    ["\u1BC5", "ba"],
    ["\u1BC7", "pa"],
    ["\u1BC9", "na"],
    ["\u1BCB", "wa"],
    ["\u1BCE", "ga"],
    ["\u1BD0", "ja"],
    ["\u1BD1", "da"],
    ["\u1BD2", "ra"],
    ["\u1BD4", "ma"],
    ["\u1BD6", "ta"],
    ["\u1BD8", "sa"],
    ["\u1BDB", "ya"],
    ["\u1BDE", "la"],

    ["\u1BC2\u1BEA", "hi"],
    ["\u1BC2\u1BEA", "ki"],
    ["\u1BC5\u1BEA", "bi"],
    ["\u1BC7\u1BEA", "pi"],
    ["\u1BC9\u1BEA", "ni"],
    ["\u1BCB\u1BEA", "wi"],
    ["\u1BCE\u1BEA", "gi"],
    ["\u1BD0\u1BEA", "ji"],
    ["\u1BD1\u1BEA", "di"],
    ["\u1BD2\u1BEA", "ri"],
    ["\u1BD4\u1BEA", "mi"],
    ["\u1BD6\u1BEA", "ti"],
    ["\u1BD8\u1BEA", "si"],
    ["\u1BDB\u1BEA", "yi"],
    ["\u1BDE\u1BEA", "li"],

    ["\u1BC2\u1BE7", "hu"],
    ["\u1BC2\u1BE7", "ku"],
    ["\u1BC5\u1BE7", "bu"],
    ["\u1BC7\u1BE7", "pu"],
    ["\u1BC9\u1BE7", "nu"],
    ["\u1BCB\u1BE7", "wu"],
    ["\u1BCE\u1BE7", "gu"],
    ["\u1BD0\u1BE7", "ju"],
    ["\u1BD1\u1BE7", "du"],
    ["\u1BD2\u1BE7", "ru"],
    ["\u1BD4\u1BE7", "mu"],
    ["\u1BD6\u1BE7", "tu"],
    ["\u1BD8\u1BE7", "su"],
    ["\u1BDB\u1BE7", "yu"],
    ["\u1BDE\u1BE7", "lu"],

    ["\u1BC2\u1BE9", "he"],
    ["\u1BC2\u1BE9", "ke"],
    ["\u1BC5\u1BE9", "be"],
    ["\u1BC7\u1BE9", "pe"],
    ["\u1BC9\u1BE9", "ne"],
    ["\u1BCB\u1BE9", "we"],
    ["\u1BCE\u1BE9", "ge"],
    ["\u1BD0\u1BE9", "je"],
    ["\u1BD1\u1BE9", "de"],
    ["\u1BD2\u1BE9", "re"],
    ["\u1BD4\u1BE9", "me"],
    ["\u1BD6\u1BE9", "te"],
    ["\u1BD8\u1BE9", "se"],
    ["\u1BDB\u1BE9", "ye"],
    ["\u1BDE\u1BE9", "le"],

    ["\u1BC2\u1BEC", "ho"],
    ["\u1BC2\u1BEC", "ko"],
    ["\u1BC5\u1BEC", "bo"],
    ["\u1BC7\u1BEC", "po"],
    ["\u1BC9\u1BEC", "no"],
    ["\u1BCB\u1BEC", "wo"],
    ["\u1BCE\u1BEC", "go"],
    ["\u1BD0\u1BEC", "jo"],
    ["\u1BD1\u1BEC", "do"],
    ["\u1BD2\u1BEC", "ro"],
    ["\u1BD4\u1BEC", "mo"],
    ["\u1BD6\u1BEC", "to"],
    ["\u1BD8\u1BEC", "so"],
    ["\u1BDB\u1BEC", "yo"],
    ["\u1BDE\u1BEC", "lo"],
]

const BatakScript : Batak[] = [
    ["\u1BC2\u1BF2", "h"],
    ["\u1BC2\u1BF2", "k"],
    ["\u1BC5\u1BF2", "b"],
    ["\u1BC7\u1BF2", "p"],
    ["\u1BC9\u1BF2", "n"],
    ["\u1BCB\u1BF2", "w"],
    ["\u1BCE\u1BF2", "g"],
    ["\u1BD0\u1BF2", "j"],
    ["\u1BD1\u1BF2", "d"],
    ["\u1BD2\u1BF2", "r"],
    ["\u1BD4\u1BF2", "m"],
    ["\u1BD6\u1BF2", "t"],
    ["\u1BD8\u1BF2", "s"],
    ["\u1BDB\u1BF2", "y"],
    ["\u1BDE\u1BF2", "l"],
    ["\u1BC0", "a"],
    ["\u1BE4", "i"],
    ["\u1BE5", "u"],
    ["\u1BC0\u1BE9", "e"],
    ["\u1BC0\u1BEC", "o"],
]

function transliterate(stringToTranslate: string, translationMap: Batak[]): 
    string {
        let translatedString: string = translationMap.reduce<string>((acc, [key, val]) => acc.replace(new RegExp(key, 'gi'), val),
        stringToTranslate.slice()
    )
    return translatedString;
}


function chainTransliterate(inputString: string, transliterationRules: Batak[][]):
    string {
        let outputString = transliterationRules.reduce<string>(
        (acc, rule) => transliterate(acc, rule),
        inputString.slice()
    )
    return outputString;
}

export function transliterating3(latinString: string): 
    string {
    let firstLetter = transliterate(latinString.charAt(0), SpecialOneScriptOne);
    return "\u200E".concat(
        chainTransliterate(firstLetter.concat(latinString.slice(1)),
                           [SpecialOneScriptOne, SpecialOneScriptTwo, SpecialScript, BatakScript]));
}