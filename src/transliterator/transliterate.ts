type Kubti = [string,  string]
type Latin = [string, string]

const SpecialScript : Kubti[] = [
    ["ks", "\u2C9D"],
    ["sy", "\u03E3"],
    ["ss", "\u03B8"],
    ["kh" , "\u2CCD\u0304\u2CCD"],
    ["h " , "\u2CCD\u0304\u0020"],
    ["ngh" , "\u2C95\u0304"],
    ["ngk" , "\u2C95\u0304"],
    ["k ", "\u2CCD\u0304\u0020"],
    ["k-", "\u2CCD\u0304"],
    ["h-", "\u2CCD\u0304"],
    ["hh", "\u2CCD\u0304\u2CCD"],
    ["ng", "\u2C9B\u0304"],
    ["NG", "\u2C9A\u0304"],
    ["ph" , "\u2CAB"],
    ["christos", "\u2CE9"],
    ["ti-coptic", "\u03EF"],
]

const KubtiScript : Kubti[] = [
    ["a" , "\u2C81"],
    ["i" , "\u2C8F"],
    ["u" , "\u2CA9"],
    ["e" , "\u2C89"],
    ["o" , "\u2C9F"],
    ["h" , "\u2CCD"],
    ["n" , "\u2C9B"],
    ["r" , "\u2CA3"],
    ["t" , "\u2CA7"],
    ["b" , "\u2C83"],
    ["m" , "\u2C99"],
    ["l" , "\u2C97"],
    ["p" , "\u2CA1"],
    ["s" , "\u2CA5"],
    ["d" , "\u2C87"],
    ["g" , "\u2C85"],
    ["j" , "\u03EB"],
    ["k" , "\u2C95"],
    ["q" , "\u2CCD\u0304"],
    ["w" , "\u2CB1"],
    ["y" , "\u2C93"],
    ["f" , "\u03E5"],
    ["z" , "\u2C8D"],
    ["x" , "\u2CAD"],
    ["c" , "\u03ED"],
    ["v" , "\u2C83\u0304"],
    ["A" , "\u2C80"],
    ["I" , "\u2C8E"],
    ["U" , "\u2CA8"],
    ["E" , "\u2C88"],
    ["O" , "\u2C9E"],
    ["H" , "\u2CCC"],
    ["N" , "\u2C9A"],
    ["R" , "\u2CA2"],
    ["T" , "\u2CA6"],
    ["B" , "\u2C82"],
    ["M" , "\u2C98"],
    ["L" , "\u2C96"],
    ["P" , "\u2CA0"],
    ["S" , "\u2CA4"],
    ["D" , "\u2C86"],
    ["G" , "\u2C84"],
    ["J" , "\u03EA"],
    ["K" , "\u2C94"],
]

function transliterate(stringToTranslate: string, translationMap: Kubti[]): 
    string {
        let translatedString: string = translationMap.reduce<string>((acc, [key, val]) => acc.replace(new RegExp(key, 'gi'), val),
        stringToTranslate.slice()
    )
    return translatedString;
}


function chainTransliterate(inputString: string, transliterationRules: Kubti[][]):
    string {
        let outputString = transliterationRules.reduce<string>(
        (acc, rule) => transliterate(acc, rule),
        inputString.slice()
    )
    return outputString;
}

export function transliterating(latinString: string): 



    string {
    let firstLetter = transliterate(latinString.charAt(0), SpecialScript);
    return "\u200E".concat(
        chainTransliterate(firstLetter.concat(latinString.slice(1)),
                           [SpecialScript, KubtiScript]));
}

