type Batak = [string,  string]


const SpecialOneScriptOne : Batak[] = [
    ["nya" , "\u1BE0"],
    ["nyi" , "\u1BE0\u1BEA"],
    ["nyu" , "\u1BE0\u1BE7"],
    ["nye" , "\u1BE0\u1BE9"],
    ["nyo" , "\u1BE0\u1BEC"],

    ["nga" , "\u1BDD"],
    ["ngi" , "\u1BDD\u1BEA"],
    ["ngu" , "\u1BDD\u1BE7"],
    ["nge" , "\u1BDD\u1BE9"],
    ["ngo" , "\u1BDD\u1BEC"],
    
]

const SpecialOneScriptTwo : Batak[] = [
    ["ny" , "\u1BE0\u1BF2"],
    ["ng" , "\u1BDD\u1BF2"],
    ["ng " , "\u1BF0\u0020"],
]

const SpecialScript : Batak[] = [
    ["ha" , "\u1BC2"],
    ["ka" , "\u1BC2"],
    ["ba" , "\u1BC5"],
    ["pa" , "\u1BC7"],
    ["na" , "\u1BC9"],
    ["wa" , "\u1BCB"],
    ["ga" , "\u1BCE"],
    ["ja" , "\u1BD0"],
    ["da" , "\u1BD1"],
    ["ra" , "\u1BD2"],
    ["ma" , "\u1BD4"],
    ["ta" , "\u1BD6"],
    ["sa" , "\u1BD8"],
    ["ya" , "\u1BDB"],
    ["la" , "\u1BDE"],

    ["hi" , "\u1BC2\u1BEA"],
    ["ki" , "\u1BC2\u1BEA"],
    ["bi" , "\u1BC5\u1BEA"],
    ["pi" , "\u1BC7\u1BEA"],
    ["ni" , "\u1BC9\u1BEA"],
    ["wi" , "\u1BCB\u1BEA"],
    ["gi" , "\u1BCE\u1BEA"],
    ["ji" , "\u1BD0\u1BEA"],
    ["di" , "\u1BD1\u1BEA"],
    ["ri" , "\u1BD2\u1BEA"],
    ["mi" , "\u1BD4\u1BEA"],
    ["ti" , "\u1BD6\u1BEA"],
    ["si" , "\u1BD8\u1BEA"],
    ["yi" , "\u1BDB\u1BEA"],
    ["li" , "\u1BDE\u1BEA"],

    ["hu" , "\u1BC2\u1BE7"],
    ["ku" , "\u1BC2\u1BE7"],
    ["bu" , "\u1BC5\u1BE7"],
    ["pu" , "\u1BC7\u1BE7"],
    ["nu" , "\u1BC9\u1BE7"],
    ["wu" , "\u1BCB\u1BE7"],
    ["gu" , "\u1BCE\u1BE7"],
    ["ju" , "\u1BD0\u1BE7"],
    ["du" , "\u1BD1\u1BE7"],
    ["ru" , "\u1BD2\u1BE7"],
    ["mu" , "\u1BD4\u1BE7"],
    ["tu" , "\u1BD6\u1BE7"],
    ["su" , "\u1BD8\u1BE7"],
    ["yu" , "\u1BDB\u1BE7"],
    ["lu" , "\u1BDE\u1BE7"],

    ["he" , "\u1BC2\u1BE9"],
    ["ke" , "\u1BC2\u1BE9"],
    ["be" , "\u1BC5\u1BE9"],
    ["pe" , "\u1BC7\u1BE9"],
    ["ne" , "\u1BC9\u1BE9"],
    ["we" , "\u1BCB\u1BE9"],
    ["ge" , "\u1BCE\u1BE9"],
    ["je" , "\u1BD0\u1BE9"],
    ["de" , "\u1BD1\u1BE9"],
    ["re" , "\u1BD2\u1BE9"],
    ["me" , "\u1BD4\u1BE9"],
    ["te" , "\u1BD6\u1BE9"],
    ["se" , "\u1BD8\u1BE9"],
    ["ye" , "\u1BDB\u1BE9"],
    ["le" , "\u1BDE\u1BE9"],

    ["ho" , "\u1BC2\u1BEC"],
    ["ko" , "\u1BC2\u1BEC"],
    ["bo" , "\u1BC5\u1BEC"],
    ["po" , "\u1BC7\u1BEC"],
    ["no" , "\u1BC9\u1BEC"],
    ["wo" , "\u1BCB\u1BEC"],
    ["go" , "\u1BCE\u1BEC"],
    ["jo" , "\u1BD0\u1BEC"],
    ["do" , "\u1BD1\u1BEC"],
    ["ro" , "\u1BD2\u1BEC"],
    ["mo" , "\u1BD4\u1BEC"],
    ["to" , "\u1BD6\u1BEC"],
    ["so" , "\u1BD8\u1BEC"],
    ["yo" , "\u1BDB\u1BEC"],
    ["lo" , "\u1BDE\u1BEC"],
]

const BatakScript : Batak[] = [
    ["h" , "\u1BC2\u1BF2"],
    ["k" , "\u1BC2\u1BF2"],
    ["b" , "\u1BC5\u1BF2"],
    ["p" , "\u1BC7\u1BF2"],
    ["n" , "\u1BC9\u1BF2"],
    ["w" , "\u1BCB\u1BF2"],
    ["g" , "\u1BCE\u1BF2"],
    ["j" , "\u1BD0\u1BF2"],
    ["d" , "\u1BD1\u1BF2"],
    ["r" , "\u1BD2\u1BF2"],
    ["m" , "\u1BD4\u1BF2"],
    ["t" , "\u1BD6\u1BF2"],
    ["s" , "\u1BD8\u1BF2"],
    ["y" , "\u1BDB\u1BF2"],
    ["l" , "\u1BDE\u1BF2"],
    ["a" , "\u1BC0"],
    ["i" , "\u1BE4"],
    ["u" , "\u1BE5"],
    ["e" , "\u1BC0\u1BE9"],
    ["o" , "\u1BC0\u1BEC"],
]

function transliterate(stringToTranslate: string, translationMap: Batak[]): 
    string {
        let translatedString: string = translationMap.reduce<string>((acc, [key, val]) => acc.replace(new RegExp(key, 'gi'), val),
        stringToTranslate.slice()
    )
    return translatedString;
}


function chainTransliterate(inputString: string, transliterationRules: Batak[][]):
    string {
        let outputString = transliterationRules.reduce<string>(
        (acc, rule) => transliterate(acc, rule),
        inputString.slice()
    )
    return outputString;
}

export function transliterating2(latinString: string): 



    string {
    let firstLetter = transliterate(latinString.charAt(0), SpecialOneScriptOne);
    return "\u200E".concat(
        chainTransliterate(firstLetter.concat(latinString.slice(1)),
                           [SpecialOneScriptOne, SpecialOneScriptTwo, SpecialScript, BatakScript]));
}