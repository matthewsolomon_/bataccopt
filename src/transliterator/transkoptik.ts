type Kubti = [string,  string]

const SpecialScript : Kubti[] = [
    ["\u2CAB", "ph"],
    ["\u03E3", "sy"],
    ["\u03B8", "ss"],
    ["\u2CCD\u0304\u2CCD", "hh"],
    ["\u2CCD\u0304", "h"],
    ["\u2CCD\u0304\u0020", "h "],
    ["\u2C95\u0304", "ngh"],
    ["\u2C9B\u0304", "ng"],
    ["\u2C9D", "ks"]
]

const KubtiScript : Kubti[] = [
    ["\u2C81", "a"],
    ["\u2C8F", "i"],
    ["\u2CA9", "u"],
    ["\u2C89", "e"],
    ["\u2C9F", "o"],
    ["\u2CCD", "h"],
    ["\u2C9B", "n"],
    ["\u2CA3", "r"],
    ["\u2CA7", "t"],
    ["\u2C83", "b"],
    ["\u2C99", "m"],
    ["\u2C97", "l"],
    ["\u2CA1", "p"],
    ["\u2CA5", "s"],
    ["\u2C87", "d"],
    ["\u2C85", "g"],
    ["\u03EB", "j"],
    ["\u2C95", "k"],
    ["\u2CCD\u0304", "q"],
    ["\u2CB1", "w"],
    ["\u2C93", "y"],
    ["\u03E5", "f"],
    ["\u2C8D", "z"],
    ["\u2CAD", "x"],
    ["\u03ED", "c"],
    ["\u2C83\u0304", "v"],
]

function transliterate(stringToTranslate: string, translationMap: Kubti[]): 
    string {
        let translatedString: string = translationMap.reduce<string>((acc, [key, val]) => acc.replace(new RegExp(key, 'gi'), val),
        stringToTranslate.slice()
    )
    return translatedString;
}


function chainTransliterate(inputString: string, transliterationRules: Kubti[][]):
    string {
        let outputString = transliterationRules.reduce<string>(
        (acc, rule) => transliterate(acc, rule),
        inputString.slice()
    )
    return outputString;
}

export function transliterating4(latinString: string): 
    string {
    let firstLetter = transliterate(latinString.charAt(0), SpecialScript);
    return "\u200E".concat(
        chainTransliterate(firstLetter.concat(latinString.slice(1)),
                           [SpecialScript, KubtiScript]));
}

